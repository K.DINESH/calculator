import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Image,
} from 'react-native';
import React, {useRef, useState} from 'react';
import HEADER from './src/components/header';

const DATA = [
  {name: 'C'},
  {name: '%'},
  {name: '<'},
  {name: '/'},
  {name: '7'},
  {name: '8'},
  {name: '9'},
  {name: '*'},
  {name: '4'},
  {name: '5'},
  {name: '6'},
  {name: '-'},
  {name: '1'},
  {name: '2'},
  {name: '3'},
  {name: '+'},
  {name: '00'},
  {name: '0'},
  {name: '.'},
  {name: '='},
];

export default function App() {
  const [enteredValue, setEnteredValue] = useState('');
  const [historyDisplay, changeDisplay] = useState('');
  const [historyData, changeHistoryData] = useState([]);

  const equalityCheck = useRef(false);
  const historyCount = useRef(1);

  function history(condition) {
    if (condition === true) {
      return changeDisplay(historyData);
    }
    return changeDisplay('');
  }

  function inputHandler(value) {
    setEnteredValue(previousValue => {
      if (equalityCheck['current'] === true) {
        if (value === 'C' || value === '<' || value === '=') {
          previousValue = '';
        } else if (isNaN(parseInt(value))) {
          previousValue += value;
        } else {
          previousValue = value;
        }
        equalityCheck.current = false;
      } else if (value === 'C') {
        previousValue = '';
      } else if (value === '<') {
        previousValue = previousValue.slice(0, -1);
      } else if (value === '=') {
        try {
          const data = eval(previousValue);
          if (data === undefined) {
            previousValue = '';
          } else {
            let tempObject = {
              key: historyCount['current'],
              ans: previousValue + ' = ' + data,
            };
            historyData.splice(0, 0, tempObject);
            changeHistoryData(historyData);
            previousValue = data;
            historyCount.current = historyCount['current'] + 1;
          }
        } catch {
          previousValue = 'error';
        }
        equalityCheck.current = true;
      } else if (
        isNaN(parseInt(value)) &&
        previousValue[previousValue.length - 1] === value
      ) {
        return previousValue;
      } else {
        previousValue += value;
      }
      return previousValue;
    });
  }

  const DisplayComp = () => {
    return (
      <View style={styles.inputBox}>
        <Text style={styles.inputBoxText}>{enteredValue}</Text>
      </View>
    );
  };

  const HistoryComp = () => {
    return (
      <View style={styles.historyView}>
        <View style={{marginLeft: 10, marginRight: 20}}>
          <TouchableOpacity onPress={() => history(!historyDisplay)}>
            <Image
              style={{width: 40, height: 40}}
              source={
                historyDisplay
                  ? require('./src/assets/cross.png')
                  : require('./src/assets/historysymbol.jpg')
              }
            />
          </TouchableOpacity>
        </View>
        {historyDisplay != '' ? (
          <FlatList
            horizontal
            data={historyDisplay}
            renderItem={({item, index}) => (
              <View style={{flex: 1, flexDirection: 'row'}}>
                <View
                  style={{
                    padding: 6,
                    borderRadius: 30,
                    ...{backgroundColor: index % 2 == 0 ? 'skyblue' : 'black'},
                  }}>
                  <Text
                    style={{
                      fontSize: 20,
                      fontWeigh: 'bold',
                      ...{color: index % 2 == 0 ? 'black' : 'white'},
                    }}>
                    {item['ans']}
                  </Text>
                </View>
                <View>
                  <Text>{'    '}</Text>
                </View>
              </View>
            )}
            keyExtractor={item => item['key']}
          />
        ) : (
          <Text></Text>
        )}
      </View>
    );
  };

  const OptionComp = () => {
    return (
      <View style={styles.valuesView}>
        <FlatList
          contentContainerStyle={{
            height: '100%',
            justifyContent: 'space-around',
            alignSelf: 'stretch',
            alignItems: 'center',
          }}
          data={DATA}
          numColumns={4}
          renderItem={({item}) => (
            <TouchableOpacity onPress={() => inputHandler(item.name)}>
              <Text style={styles.values}>{item.name}</Text>
            </TouchableOpacity>
          )}
        />
      </View>
    );
  };

  return (
    <View style={styles.header}>
      <HEADER />
      <DisplayComp />
      <HistoryComp />
      <OptionComp />
    </View>
  );
}

const styles = StyleSheet.create({
  header: {backgroundColor: 'white', flex: 1},
  inputBox: {
    flex: 0.2,
    alignItems: 'flex-end',
    justifyContent: 'center',
    borderWidth: 2,
    borderLeftWidth: 4,
    borderRightWidth: 4,
    borderColor: 'black',
  },
  inputBoxText: {fontSize: 40, fontWeight: 'bold', color: 'black'},
  historyView: {
    flex: 0.13,
    flexDirection: 'row',
    borderColor: 'black',
    borderWidth: 2,
    borderBottomWidth: 0,
    alignItems: 'center',
  },
  valuesView: {
    flex: 1,
    backgroundColor: 'skyblue',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: 'black',
  },
  values: {
    color: 'black',
    fontSize: 22,
    fontWeight: 'bold',
    margin: 44,
  },
});
