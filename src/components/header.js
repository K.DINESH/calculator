import {View, Text, StyleSheet} from 'react-native';
import React from 'react';

export default function HEADER() {
  return (
    <View style={styles.header}>
      <Text style={styles.headerText}>CALCULATOR</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  header: {
    flex: 0.05,
    backgroundColor: 'skyblue',
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 2,
  },
  headerText: {
    fontSize: 20,
    alignContent: 'center',
    fontWeight: 'bold',
    color: 'black',
  },
});
